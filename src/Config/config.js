const config = env => {
  if (env === "development") {
    return {
      protocol: "http://",
      // protocol: "https://",
      baseurl: "localhost:3000",
      // baseurl: "api.cheappr.io",
      // baseurl: "api.cheappr.io",
      alldealspath: "/alldeals",
      publishPath: "/publish",
      getPublished: "/viewpub",
      login: "/google",
      user: "/user"
    };
  }
  if (env === "production") {
    return {
      protocol: "https://",
      baseurl: "api.cheappr.io",
      alldealspath: "/alldeals",
      publishPath: "/publish",
      getPublished: "/viewpub",
      login: "/google",
      user: "/user"
    };
  }
};

export default config;
