const Images = {
  thailand: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/thailand.jpg"
  ],
  australia: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/australia.jpg"
  ],
  default: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/cities.jpg",
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/traveller.jpg"
  ],
  dubai: ["https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/dubai1.jpg"],
  europe: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/europe.jpg"
  ],
  china: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/hongkong1.jpg",
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/hongkong2.jpg"
  ],
  maldives: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/maldives1.jpg"
  ],
  singapore: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/singapore1.jpg"
  ],
  germany: [
    "https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/germany.jpg"
  ],
  london: ["https://cheappr.sgp1.digitaloceanspaces.com/Cheappr_Web/london.jpg"]
};

export const getImage = key => {
  const images = Images[key.toLowerCase()];
  if (images) {
    return images[0];
  } else {
    return Images["default"][0];
  }
};
