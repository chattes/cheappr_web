import { h, Component } from "preact";
import { Router, route } from "preact-router";

import Header from "./header";

// Code-splitting is automated for routes
import Home from "../routes/home";
import Profile from "../routes/profile";
import Logged from "../routes/logged";
import UserHome from "../routes/user_home";
import Admin from "../routes/admin";
import config from "../Config/config";

const env = process.env.NODE_ENV || "development";
const urls = config(env);

export default class App extends Component {
  /** Gets fired when the route changes.
   *	@param {Object} event		"change" event from [preact-router](http://git.io/preact-router)
   *	@param {string} event.url	The newly routed URL
   */
  isAuthenticated = () => {
    const userURL = `${urls.protocol}${urls.baseurl}${urls.user}`;
    return fetch(userURL, {
      method: "get",
      headers: new Headers({
        Authorization: window.localStorage.getItem("token")
      })
    })
      .then(response => response.json())
      .then(data => {
        if (data.statusCode == 401) {
          return {
            isAuthenticated: false,
            role: null
          };
        }
        return {
          isAuthenticated: true,
          role: data.role
        };
      })
      .catch(error => ({
        isAuthenticated: false,
        role: null
      }));
  };
  handleRoute = async e => {
    this.currentUrl = e.url;
    const { isAuthenticated, role } = await this.isAuthenticated();
    switch (this.currentUrl) {
      case "/":
        if (isAuthenticated && role === "ADMIN") {
          return route("/admin", true);
        }
        if (isAuthenticated && role !== "ADMIN") {
          return route("/userdeals", true);
        }

        return route("/");

        break;

      case "/logged":
        if (!isAuthenticated) {
          return route("/", true);
        }
        if (role == "ADMIN") {
          return route("/admin", true);
        }
        return route("/userdeals", true);
        break;

      case "/admin":
        if (!isAuthenticated) {
          return route("/", true);
        }

        if (role !== "ADMIN") {
          return route("/userdeals");
        }

        break;

      case "/userdeals":
        if (!isAuthenticated) {
          return route("/", true);
        }

        break;
    }
  };

  render() {
    return (
      <div id="app">
        <Header />
        <Router onChange={this.handleRoute}>
          <Home path="/" />
          <Logged path="/logged" />
          <Profile path="/profile/" user="me" />
          <Profile path="/profile/:user" />
          <Admin path="/admin" />
          <UserHome path="/userdeals" />
        </Router>
      </div>
    );
  }
}
