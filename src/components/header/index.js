import { h } from "preact";
import { Link } from "preact-router/match";
import style from "./style";

const Header = () => (
  <header class={style.header}>
    <img src={require("../../assets/icons/White_png.png")} />
  </header>
);

export default Header;
