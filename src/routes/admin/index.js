import style from "./style";
import { h, Component } from "preact";
import config from "../../Config/config";
import dayjs from "dayjs";
import { Button, Modal, TextField } from "preact-fluid";
import Notifications, { notify } from "react-notify-toast";
import PublishDeal from "./publish";
import debounce from "lodash.debounce";
import { window, document } from "global";
import { Pulsate } from "styled-loaders";
import { Target } from "preact-feather";

// const R = require("ramda");
const R = require("rambda");

const env = process.env.NODE_ENV || "development";
const urls = config(env);

var timer;

var windows = [];

const deepLinks = ({ flyFrom, flyTo, dtimetrip1, dtimetrip2 }) => {
  const dep_date1_google = dayjs.unix(dtimetrip1).format("YYYY-MM-DD");
  const dep_date2_google = dayjs.unix(dtimetrip2).format("YYYY-MM-DD");
  const dep_dat1_sky = dayjs.unix(dtimetrip1).format("YYMMDD");
  const dep_dat2_sky = dayjs.unix(dtimetrip2).format("YYMMDD");
  const deep_link_google = `https://www.google.com/flights?hl=en#flt=${flyFrom}.${flyTo}.${dep_date1_google}*${flyTo}.${flyFrom}.${dep_date2_google};c:INR;e:1;sd:1;t:f`;
  const deep_link_kayak = `https://www.kayak.co.in/flights/${flyFrom}-${flyTo}/${dep_date1_google}/${dep_date2_google}?sort=price_a`;
  const deep_link_skyscanner = `https://www.skyscanner.co.in/transport/flights/${flyFrom.toLowerCase()}/${flyTo.toLowerCase()}/${dep_dat1_sky}/${dep_dat2_sky}/?adults=1&children=0&adultsv2=1&childrenv2=&infants=0&cabinclass=economy&rtn=1&preferdirects=false&outboundaltsenabled=false&inboundaltsenabled=false&ref=home#results`;
  return {
    deep_link_google,
    deep_link_kayak,
    deep_link_skyscanner
  };
};

class Admin extends Component {
  state = {
    next: null,
    deals: [],
    loading: false
  };
  constructor() {
    super();
    this.modal = null;
  }
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);

    const dealsUrl = `${urls.protocol}${urls.baseurl}${urls.alldealspath}`;
    fetch(dealsUrl, {
      method: "get",
      headers: new Headers({
        Authorization: window.localStorage.getItem("token")
      })
    })
      .then(resp => resp.json())
      .then(data => {
        const { deals } = this.state;
        const new_deals = [...deals, ...data.results];
        this.setState({
          deals: new_deals,
          next: data.meta.next
        });
      });
  }

  handleScroll = debounce(() => {
    if (!this.state.next || this.state.loading) return;
    // Checks that the page has scrolled to the bottom
    const Height1 = window.innerHeight + document.documentElement.scrollTop;
    const Height2 = document.body.scrollHeight;
    const scrollDiff = Height2 - Height1;
    console.log("Sourav Logging:::: Scroll Diff::", scrollDiff);
    if (scrollDiff == 0) {
      console.log("Sourav Logging:::: Loading More Deals...");
      this.loadMoreDeals();
    }
  }, 500);

  // handleScroll = debounce(() => {
  //   if (!this.state.next || this.state.loading) return;
  //   // Checks that the page has scrolled to the bottom
  //   const Height1 = window.innerHeight + document.documentElement.scrollTop;
  //   const Height2 = document.body.scrollHeight;
  //   // const scrollDiff = Height2 - Height1;
  //   console.log("Sourav Logging:::: Scroll Diff::", scrollDiff);
  //   if (scrollDiff == 0) {
  //     console.log("Sourav Logging:::: Loading More Deals...");
  //     this.loadMoreDeals();
  //   }
  // }, 500);

  loadMoreDeals = () => {
    const dealsUrl = `${urls.protocol}${urls.baseurl}${this.state.next}`;
    this.setState({ loading: true });
    fetch(dealsUrl, {
      method: "get",
      headers: new Headers({
        Authorization: window.localStorage.getItem("token")
      })
    })
      .then(resp => resp.json())
      .then(data => {
        const { deals } = this.state;
        const new_deals = [...deals, ...data.results];
        this.setState({
          deals: new_deals,
          next: data.meta.next,
          loading: false
        });
      })
      .catch(error => {
        this.setState({
          deals: [...this.state.deals, ...[]],
          next: null,
          loading: false
        });
      });
  };
  showModal = ({ deal_data }) => {
    this.modal.show(
      <PublishDeal deal_data={deal_data} close={this.closeModal} />
    );
  };

  closeModal = () => this.modal.hide();
  render() {
    const deals_grouped = group_deals(this.state.deals);

    return (
      <div className={style.home}>
        <div className={style.toolbox}>
          <h3>Tools</h3>
          <div>
            <Button
              onClick={() => {
                window.open(
                  "https://www.skyscanner.co.in/transport/flights-from/in/?adults=1&children=0&adultsv2=1&childrenv2=&infants=0&cabinclass=economy&rtn=1&preferdirects=false&outboundaltsenabled=false&inboundaltsenabled=false&ref=home"
                );
              }}
              primary={true}
            >
              Search Sky Scanner
            </Button>
          </div>

          <div className={style.toolbtn}>
            <Button
              onClick={() => {
                window.open(
                  "https://www.google.com/flights?hl=en#flt=DEL,BLR,COK,JAI,CCU,BOM,HYD..2019-08-13*.DEL,BLR,COK,JAI,CCU,BOM,HYD.2019-08-17;c:INR;e:1;ls:1w;p:36000.0.INR;sd:0;er:-201672876.20948304.670554695.-1779051696;t:e"
                );
              }}
              primary={true}
            >
              Search Google
            </Button>
          </div>
          <div className={style.toolbtn}>
            <Button
              onClick={() => {
                this.showModal({
                  deal_data: {
                    countryTo: { code: "ZZ", name: "Enter Country Name" },
                    countryFrom: { code: "IN", name: "India" }
                  }
                });
              }}
              primary={true}
            >
              Publish a Deal
            </Button>
          </div>
        </div>

        <DealDate deals={deals_grouped} publish={this.showModal} />
        <Modal id="publish-1" ref={ref => (this.modal = ref)} />
        <div className={style.loader}>
          {this.state.loading ? <Pulsate color="white" /> : null}
        </div>
      </div>
    );
  }
}

const byDate = R.groupBy(deal =>
  dayjs(deal.createdAt)
    .format("DD/MM/YYYY")
    .toString()
);
const group_deals = deals => {
  const gr_deals = byDate(deals);
  console.log("Sourav Logging:::: Grouped", gr_deals);
  return gr_deals;
};

const DealDate = ({ deals, publish }) => {
  const dealKeys = R.keys(deals);
  return (
    <div>
      {dealKeys.map(dealKey => {
        const dealsByDate = deals[dealKey];
        return (
          <div>
            <h1>
              Deals for <span>{dealKey}</span>
            </h1>
            <Deal deals_by_date={dealsByDate} publish={publish} />
          </div>
        );
      })}
    </div>
  );
};
const Deal = ({ deals_by_date, publish }) => {
  return (
    <div>
      {deals_by_date.map(deal => (
        <div className={style.deal}>
          <p>
            {deal.deal_data.nightsInDest} Nights in{" "}
            <strong>{deal.deal_data.countryTo.name}</strong>
            <small>({deal.deal_data.cityTo})</small>
          </p>
          <span>
            Price <strong>{deal.deal_data.price}</strong>
          </span>
          <p>Average Price {deal.deal_data.usual_price}</p>
          <div className={style.buttonContainer}>
            <Button
              secondary={true}
              onClick={e => {
                e.preventDefault();
                windows.push(window.open(deal.deal_data.deep_link));
                const {
                  deep_link_google,
                  deep_link_kayak,
                  deep_link_skyscanner
                } = deepLinks({
                  flyFrom: deal.deal_data.icao_from,
                  flyTo: deal.deal_data.icao_to,
                  dtimetrip1: deal.deal_data.dtimetrip1,
                  dtimetrip2: deal.deal_data.dtimetrip2
                });
                windows.push(window.open(deep_link_google));
                windows.push(window.open(deep_link_kayak));
                windows.push(window.open(deep_link_skyscanner));
              }}
            >
              Compare
            </Button>
            <Button
              primary={true}
              onClick={() => {
                windows.forEach(window => window.close());
                windows = [];
              }}
            >
              Close
            </Button>
          </div>
          <div className={style.publishcontainer}>
            <Button
              className={style.publishbtn}
              primary={true}
              onClick={e => {
                e.preventDefault();
                publish({ deal_data: deal.deal_data });
              }}
            >
              Publish
            </Button>
          </div>
        </div>
      ))}
    </div>
  );
};

export default Admin;
