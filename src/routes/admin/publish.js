import style from './style';
import { h, Component } from 'preact';
import config from '../../Config/config';
import dayjs from 'dayjs';
import { Button, Modal, TextField, Checkbox } from 'preact-fluid';
import Notifications, { notify } from 'react-notify-toast';
import Textarea from 'react-textarea-autosize';
import Editor from 'react-pell';

// const R = require("ramda");
const R = require('rambda');

const env = process.env.NODE_ENV || 'development';
const urls = config(env);

let windows = [];

class PublishDeal extends Component {
	state = {
		premium: false,
		notesHtml: `<p>Write Notes</p>`
	};
	constructor() {
		super();
	}
	componentDidMount() {
		let { deal_data } = this.props;
		deal_data.notes = `<p>Notes Here</p>`;
		this.setState({ deal_data });
	}

	handlePublish = ({
		city_from,
		city_to,
		country,
		price,
		usual_price,
		link,
		premium,
		nights,
		notes,
		image_url,
		when,
		airlines
	}) => {
		const publishURL = `${urls.protocol}${urls.baseurl}${urls.publishPath}`;
		fetch(publishURL, {
			method: 'POST',
			headers: new Headers({
				Authorization: window.localStorage.getItem('token'),
				Accept: 'application/json',
				'Content-Type': 'application/json'
			}),
			body: JSON.stringify({
				cityFrom: city_from,
				cityTo: city_to,
				countryFrom: 'India',
				countryTo: country,
				price,
				usual_price,
				link,
				premium,
				nights,
				notes,
				image_url,
				when,
				airlines
			})
		})
			.then(resp => resp.json())
			.then(data => {
				if (data == 200) {
					notify.show('Deal Published Successfuly', 'success');
					return;
				}
				if (data.statusCode == 401) {
					notify.show('Session Expired... Login again', 'warning');
					return;
				}
				notify.show('Snap! Cannot Publish Deal. Error Occured', 'error');
				console.log('Sourav Logging:::: ', data.message);
				return;
			})
			.catch(error => {
				console.log('Sourav Logging:::: Error', error);

				notify.show('Error publishing deal', 'error');
			});
	};

	onCityFromChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, cityFrom: input.target.value };
		this.setState({ deal_data: new_state });
	};
	onCityToChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, cityTo: input.target.value };
		this.setState({ deal_data: new_state });
	};
	onCountryFromChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = {
			...deal_data,
			countryFrom: { code: 'ZZ', name: input.target.value }
		};
		this.setState({ deal_data: new_state });
	};
	onCountryToChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = {
			...deal_data,
			countryTo: { code: 'ZZ', name: input.target.value }
		};
		this.setState({ deal_data: new_state });
	};
	onLinkChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, deep_link: input.target.value };
		this.setState({ deal_data: new_state });
	};

	onPriceChaged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, price: input.target.value };
		this.setState({ deal_data: new_state });
	};

	onUsusalPriceChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, usual_price: input.target.value };
		this.setState({ deal_data: new_state });
	};
	onNightsChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, nightsInDest: input.target.value };
		this.setState({ deal_data: new_state });
	};

	onWhenChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, when: input.target.value };
		this.setState({ deal_data: new_state });
	};

	onAirlinesChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, airlines: input.target.value };
		this.setState({ deal_data: new_state });
	};

	onNotesChanged = html => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, notes: html };
		this.setState({ deal_data: new_state });
	};

	onImageChanged = input => {
		const deal_data = this.state.deal_data;
		const new_state = { ...deal_data, image_url: input.target.value };
		this.setState({ deal_data: new_state });
	};

	togglePremium = () => this.setState({ premium: !this.state.premium });

	render() {
		console.log('Sourav Logging:::: Current Deal', this.state);
		const { close } = this.props;
		if (!this.state.deal_data) return <div />;
		const { deal_data } = this.state;
		return (
			<div className={style.modal}>
				<h3>Publish Deal</h3>
				<Checkbox
					checked={this.state.premium}
					value="light"
					label="Premium"
					onChange={this.togglePremium}
				/>
				<div className="form-wrapper">
					<TextField
						label="From"
						value={deal_data.cityFrom}
						onChange={this.onCityFromChanged}
					/>
					<TextField
						label="To"
						value={deal_data.cityTo}
						onChange={this.onCityToChanged}
					/>
					<TextField
						label="Country"
						value={deal_data.countryTo.name}
						onChange={this.onCountryToChanged}
					/>
					<TextField
						label="Nights"
						value={deal_data.nightsInDest}
						onChange={this.onNightsChanged}
					/>
					<TextField
						label="Price"
						value={deal_data.price}
						disabled={false}
						onChange={this.onPriceChaged}
					/>
					<TextField
						label="Usual Price"
						value={deal_data.usual_price}
						onChange={this.onUsusalPriceChanged}
					/>
					<TextField
						label="Link"
						value={deal_data.deep_link}
						onChange={this.onLinkChanged}
					/>
					<TextField
						label="When"
						value={deal_data.when}
						onChange={this.onWhenChanged}
					/>
					<TextField
						label="Airlines"
						value={deal_data.airlines}
						onChange={this.onAirlinesChanged}
					/>
					<TextField
						label="Image"
						value={deal_data.image_url || ''}
						onChange={this.onImageChanged}
					/>

					<div className={style.myeditor}>
						<Editor
							defaultContent={deal_data.notes}
							onChange={this.onNotesChanged}
						/>
					</div>
				</div>
				<div className={style.publishbtnctr}>
					<Button
						primary
						onClick={() => {
							this.handlePublish({
								city_from: deal_data.cityFrom,
								city_to: deal_data.cityTo,
								country: deal_data.countryTo.name,
								price: deal_data.price,
								usual_price: deal_data.usual_price,
								link: deal_data.deep_link,
								premium: this.state.premium, //For Now,
								nights: deal_data.nightsInDest,
								notes: deal_data.notes,
								image_url: deal_data.image_url,
								airlines: deal_data.airlines,
								when: deal_data.when
							});
						}}
					>
						Publish
					</Button>
					<Button secondary onClick={close}>
						Cancel
					</Button>
				</div>
				<Notifications options={{ zIndex: 200, top: '50px' }} />
			</div>
		);
	}
}

export default PublishDeal;
