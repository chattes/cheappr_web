import style from "./style";
import { h, Component } from "preact";
import config from "../../Config/config";
import dayjs from "dayjs";
import { Button, Modal, TextField } from "preact-fluid";
import Notifications, { notify } from "react-notify-toast";
import { route } from "preact-router";
import { Pulsate } from "styled-loaders";
import { getImage } from "../../Config/images";
import { Lock } from "preact-feather";
import debounce from "lodash.debounce";
import { window, document } from "global";

const colors = ["#1F2833", "#C5C6C7", "#66FCF1", "#45A29E"];

// const R = require("ramda");
const R = require("rambda");

const env = process.env.NODE_ENV || "development";
const urls = config(env);

class UserHome extends Component {
  state = {
    deals: [],
    loading: false,
    next: null
  };
  constructor() {
    super();
  }
  componentDidMount() {
    window.addEventListener("scroll", this.handleScroll);
    this.setState({ loading: true });
    this.fetchDeals()
      .then(response => response.json())
      .then(data => {
        console.log(data);
        this.setState({
          loading: false,
          deals: data.results,
          next: data.meta.next
        });
      })
      .catch(error => {
        notify.show("Cannot get any deals", "error");
        this.setState({ loading: false });
      });
  }

  fetchDeals = () => {
    const publishURL = `${urls.protocol}${urls.baseurl}${urls.alldealspath}`;
    return fetch(publishURL, {
      method: "GET",
      headers: new Headers({
        Authorization: window.localStorage.getItem("token"),
        Accept: "application/json",
        "Content-Type": "application/json"
      })
    });
  };

  handleScroll = debounce(() => {
    if (!this.state.next || this.state.loading) return;
    // Checks that the page has scrolled to the bottom
    const Height1 = window.innerHeight + document.documentElement.scrollTop;
    const Height2 = document.body.scrollHeight;
    const scrollDiff = Height2 - Height1;
    console.log("Sourav Logging:::: Scroll Diff::", scrollDiff);
    if (scrollDiff == 0) {
      console.log("Sourav Logging:::: Loading More Deals...");
      this.loadMoreDeals();
    }
  }, 500);

  loadMoreDeals = () => {
    const dealsUrl = `${urls.protocol}${urls.baseurl}${this.state.next}`;
    this.setState({ loading: true });
    fetch(dealsUrl, {
      method: "get",
      headers: new Headers({
        Authorization: window.localStorage.getItem("token")
      })
    })
      .then(resp => resp.json())
      .then(data => {
        const { deals } = this.state;
        const new_deals = [...deals, ...data.results];
        this.setState({
          deals: new_deals,
          next: data.meta.next,
          loading: false
        });
      })
      .catch(error => {
        this.setState({
          deals: [...this.state.deals, ...[]],
          next: null,
          loading: false
        });
      });
  };

  render() {
    const { loading, deals } = this.state;
    if (this.state.loading) {
      <Pulsate className={style.userHome} />;
    }
    return (
      <div className={style.userHome}>
        <h1>Latest Deals from India</h1>
        <h3>
          Enjoy these great Deals while our flight experts search for good
          deals. Do check back this page regularly for daily deals on Flights.
        </h3>
        <p>
          We don't get any commissions from any travel site. Our only target is
          to give you a great deal on Interational flights!
        </p>

        <h2>Your Deals</h2>

        <div className={style.dealArea}>
          {deals.map(deal => {
            return <CheapprDeal deal_1={deal} />;
          })}
        </div>
      </div>
    );
  }
}

const CheapprDeal = ({ deal_1 }) => {
  const { deal_data: deal } = deal_1;
  return (
    <div
      className={style.dealCard}
      onClick={() => {
        if (deal.expired) return;
        window.open(deal.deep_link);
      }}
    >
      {deal.premium ? (
        <div className={style.premium}>
          <Lock size={14} /> <span>Premium</span>
        </div>
      ) : null}

      {deal.expired ? <div className={style.pastdeal}>PAST DEAL</div> : null}

      <h3>{deal.countryTo}</h3>
      <p>
        <strong>{deal.nightsInDest}</strong> nights in {deal.cityTo} and return
        for &#8377;<strike>{deal.usual_price}</strike>
      </p>

      <h3> &#8377; {Math.round(deal.price)}</h3>
      <Button secondary style={{ padding: 10 }}>
        Book Now{" "}
      </Button>
    </div>
  );
};

export default UserHome;
