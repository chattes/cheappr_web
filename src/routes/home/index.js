import { h, Component } from "preact";
import { Card, CardHeader, CardBody, TextField } from "preact-fluid";
import style from "./style";
import { GoogleLogin } from "react-google-login";
import config from "../../Config/config";
import Notifications, { notify } from "react-notify-toast";
import { error } from "util";
import { route } from "preact-router";
import Button from "preact-fluid/lib/Button";

const env = process.env.NODE_ENV || "development";
const urls = config(env);

export default class Home extends Component {
  state = {
    email: ""
  };
  // gets called when this route is navigated to
  componentDidMount() {
    // start a timer for the clock:
  }

  // gets called just before navigating away from the route
  componentWillUnmount() {}

  handleChange = input => {
    console.log(input.target.value);
    this.setState({ email: input.target.value });
  };

  authenticateGoogleLogin = () => {
    const loginURL = `${urls.protocol}${urls.baseurl}${urls.login}`;
    const { email } = this.state;
    fetch(loginURL, {
      method: "POST",
      headers: new Headers({
        // authorization: code
        authorization: "TEMP",

        Accept: "application/json",
        "Content-Type": "application/json"
      }),
      body: JSON.stringify({
        email
      })
    })
      .then(response => {
        console.log("Response from Server", response);
        if (response.statusText === "OK" || response.status == 200) {
          return response.json();
        } else {
          throw Error("Cannot Login");
        }
      })
      .then(data => {
        const { jwttoken, created } = data;
        window.localStorage.setItem("token", jwttoken);
        if (created) {
          notify.show("Welcome", "success");
          setTimeout(() => route("/logged", true), 2000);
        } else {
          notify.show("Welcome Back", "success");
          setTimeout(() => route("/logged", true), 2000);
        }

        console.log(data);
      })
      .catch(error => {
        notify.show("Cannot Login", "error");
      });
  };

  render() {
    return (
      <div className={style.home}>
        <div className={style.imagectr} />
        <div className={style.overlay}>
          <h2>
            Get International Airline tickets at up to 70% discounts delivered
            to your email{" "}
          </h2>
          <p>
            Get email alerts about cheap flights from India to top foreign
            destinations and travel like a Cheappr!
          </p>

          {/* <GoogleLogin
            clientId="774378232330-03ahh5pid2caqtgti6cupigjbg61mdd2.apps.googleusercontent.com"
            buttonText="Login"
            accessType="online"
            buttonText="Sign UP/Login"
            theme="dark"
            onSuccess={async response => {
              console.log("Sourav Logging:::: Google Login", response);
              this.authenticateGoogleLogin(response.tokenId);
            }}
            onFailure={(error, details) => {
              console.log("Sourav Logging:::: Login google Failure", details);
              notify.show("Snap something went wrong!", "error");
            }}
            cookiePolicy={"single_host_origin"}
          /> */}
        </div>
        <div className={style.signup}>
          <p>Enter email to start getting Cheap Flights!</p>
          <TextField
            placeholder="Enter your email"
            effect="border"
            onChange={this.handleChange}
            className={style.signupText}
            value={this.state.email}
            type="email"
            cell={{
              middle: true,
              width: 50
            }}
          />
          <Button
            rounded={true}
            className={style.signupbtn}
            onClick={this.authenticateGoogleLogin}
          >
            Sign Up/Sign In
          </Button>
        </div>
        <div className={style.content}>
          <h1>How Does It Work</h1>
        </div>
        <div className={style.contenthow}>
          <Card className={style.howcard}>
            <CardHeader title="Register" />
            <CardBody>
              SignUp to Cheappr with your email address. We will use this same
              email to send you deals.
            </CardBody>
          </Card>
          <Card className={style.howcard}>
            <CardHeader title="Cheappr Searches" />
            <CardBody>
              Cheappr searches for Flight Deals and Mistake fares all day every
              day(that's what we do and nothing else!)
            </CardBody>
          </Card>
          <Card className={style.howcard}>
            <CardHeader title="Get Deals" />
            <CardBody>
              Cheapr will send you the Cheapest Ticket deals to your mail inbox
              with directions of how to get the deal.
            </CardBody>
          </Card>
          <Card className={style.howcard}>
            <CardHeader title="Profit" />
            <CardBody>
              Book at almost 50~70% of usual price. Save on your flight tickets.
              Travel CHEAPPR!
            </CardBody>
          </Card>
        </div>
        <div className={style.content}>
          <h3>Some Deals we found</h3>
        </div>
        <div className={style.pricehow}>
          <img
            src={require("../../assets/icons/ExampleDeal1.png")}
            className={style.exampledealimage}
          />
          <img
            src={require("../../assets/icons/ExampleDeal2.png")}
            className={style.exampledealimage}
          />
        </div>
        <div className={style.content}>
          <div className={style.googleLogin}>
            {/* <GoogleLogin
              clientId="774378232330-03ahh5pid2caqtgti6cupigjbg61mdd2.apps.googleusercontent.com"
              theme="dark"
              buttonText="Login"
              accessType="online"
              buttonText="I want to try Cheappr! Sign me up"
              onSuccess={async response => {
                console.log("Sourav Logging:::: Google Login", response);
                this.authenticateGoogleLogin(response.tokenId);
              }}
              onFailure={() => {
                console.log("Sourav Logging:::: Login google Failure");
                notify.show("Snap something went wrong!", "error");
              }}
              cookiePolicy={"single_host_origin"}
            /> */}
          </div>
        </div>

        {/* <div className={style.content}>
          <h3>Pricing</h3>
        </div>

        <div className={style.pricehow}>
          <Card className={style.priceCard}>
            <h2 className={style.priceCard}>&#x20B9; 0/year</h2>
            <div className={style.googleLogin}>
              <GoogleLogin
                clientId="774378232330-03ahh5pid2caqtgti6cupigjbg61mdd2.apps.googleusercontent.com"
                theme="dark"
                buttonText="Login"
                accessType="online"
                buttonText="Login with Google"
                onSuccess={async response => {
                  console.log("Sourav Logging:::: Google Login", response);
                  this.authenticateGoogleLogin(response.tokenId);
                }}
                onFailure={() => {
                  console.log("Sourav Logging:::: Login google Failure");
                  notify.show("Snap something went wrong!", "error");
                }}
                cookiePolicy={"single_host_origin"}
              />
            </div>

            <CardBody>
              <ul>
                <li>Less number of Deals every week</li>
                <li>Some deal emails might be delayed</li>
                <li>Occasional Ads</li>
              </ul>
            </CardBody>
          </Card>

          <Card className={style.priceCard}>
            <h2 className={style.priceCard}>
              <strike>&#x20B9; 499/year</strike>
              <span>Free! During Beta</span>
            </h2>
            <div className={style.googleLogin}>
              <GoogleLogin
                clientId="774378232330-03ahh5pid2caqtgti6cupigjbg61mdd2.apps.googleusercontent.com"
                theme="dark"
                buttonText="Login"
                accessType="online"
                buttonText="Login with Google"
                onSuccess={async response => {
                  console.log("Sourav Logging:::: Google Login", response);
                  this.authenticateGoogleLogin(response.tokenId);
                }}
                onFailure={() => {
                  console.log("Sourav Logging:::: Login google Failure");
                  notify.show("Snap something went wrong!", "error");
                }}
                cookiePolicy={"single_host_origin"}
              />
            </div>

            <CardBody>
              <ul>
                <li>Access to all deals</li>
                <li>
                  Emails as soon as a deal is found with no or minimal delays
                </li>
                <li>No Ads</li>
                <li>Support</li>
              </ul>
            </CardBody>
          </Card>
        </div> */}
        <div className={style.footer}>
          <p>Copyright © 2019 Cheappr Labs.</p>
          <p>
            <a href="https://twitter.com/Cheappr3" target="_blank">
              Follow Us
            </a>
          </p>
          <p>See any issues? </p>
          <p>
            Send us an Email @
            <a href="mailto:travel@cheappr.io" target="_blank">
              Cheappr Support
            </a>
          </p>
        </div>
        <Notifications options={{ zIndex: 200, top: "50px" }} />
      </div>
    );
  }
}
